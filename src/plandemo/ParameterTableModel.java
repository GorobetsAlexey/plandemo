/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.util.StringTokenizer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author alexxx
 */
public class ParameterTableModel extends AbstractTableModel{

    public int getRowCount() {
        
        return MainPanel.parameters.size();
    }

    public int getColumnCount() {
        return 4;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex < MainPanel.parameters.size()) {
            Parameter cur = MainPanel.parameters.get(rowIndex);
        
        if (columnIndex == 0)
            return cur.description;
        if (columnIndex == 1)
            return cur.goodMin+"; "+cur.goodMax;
        if (columnIndex == 2)
            return cur.badMin+"; "+cur.badMax;
        if (columnIndex == 3)
            return cur.min+"; "+cur.max;
        return null;
        }
        return null;
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
	return true;
    }
    
    @Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (rowIndex < MainPanel.parameters.size())  {
            Parameter cur = MainPanel.parameters.get(rowIndex);
        if (columnIndex == 0)
            cur.description = (String)aValue;
        if (columnIndex == 1){
            StringTokenizer parser = new StringTokenizer((String)aValue, "; ");
            if (parser.countTokens() == 2) {
                cur.goodMin = Double.parseDouble(parser.nextToken());
                cur.goodMax = Double.parseDouble(parser.nextToken());            
            }
        }
        if (columnIndex == 2){
            StringTokenizer parser = new StringTokenizer((String)aValue, "; ");
            if (parser.countTokens() == 2) {
                cur.badMin = Double.parseDouble(parser.nextToken());
                cur.badMax = Double.parseDouble(parser.nextToken());            
            }
        }
        if (columnIndex == 3){
            StringTokenizer parser = new StringTokenizer((String)aValue, "; ");
            if (parser.countTokens() == 2) {
                cur.min = Double.parseDouble(parser.nextToken());
                cur.max = Double.parseDouble(parser.nextToken());            
            }
        }        
        }
    }
    
    @Override
    public String getColumnName(int column) {
		if (column == 0)
			return "Parametr";
		if (column == 1)
			return "Normal value";
		if (column == 2)
			return "Bad value";
                if (column == 3)
			return "Min; Max value";
		return "";
    }
}