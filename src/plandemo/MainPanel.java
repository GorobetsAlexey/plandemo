/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author alexxx
 */
public class MainPanel extends JPanel{
    private static  MainPanel instanse;
    public static ArrayList<Shape> plan = new ArrayList<Shape>();
  // public static ArrayList<Shape> headers = new ArrayList<Shape>();
    public static ArrayList<Parameter> parameters = new ArrayList<Parameter>();
    public static double scale = 1; 
    public static int addShapeType = Shape.COMPLEX_SHAPE;
    public static Shape buffShape;
    public static Shape currShape;
    public static boolean saved = true;
    public static boolean grid = false;
    public static Color currColor = Color.BLUE;
    
    public MainPanel () {
        super ();
        plan.add(new Room(100, 100, 160, 160));
        plan.add(new Room(260, 80, 100, 360));
        plan.add(new Room(180, 260, 305, 290));
        ((Room)plan.get(2)).addAngle(new Angle (74, 0, 6, 6, plan.get(2)));
        ((Room)plan.get(2)).addAngle(new Angle(299, 174, 6, 6, plan.get(2)));
        Angle tmp = ((Room)plan.get(2)).getAngle(305, 0);
        tmp.x = 74;
        tmp.y = 174;
        plan.add(new Room(360, 50, 440, 385));
        plan.add(new Door(205, 255, 35, 13));
        plan.get(4).flip(Shape.Y_AXIS);
        plan.add(new Door(243, 295, 35, 13));
        plan.get(5).rotate(-Math.PI/2);
        plan.add(new Door (461, 485, 45, 7));
        plan.get(6).rotate(Math.PI/2);
        plan.add(new Door (400, 428, 35, 13));
        plan.add(new Window(500, 49, 150, 8));
        plan.add(new Window(53, 175, 100, 8));
        plan.get(9).rotate(Math.PI/2);
        plan.add(new Column(450, 150, 40, 40));
        plan.add(new Column(650, 300, 40, 40));
        plan.add(new Parameter(150, 150, -120, 120, -90, 90, -60, 60, 54));
        plan.add(new Parameter(450, 250, -234, 234, -100, 100, -30, 30, 42));
        plan.add(new Parameter(700, 350, -90, 90, -50, 5, -40, 0, 10)); 
        plan.add(new ComplexShape(10, 10, 300, 300));
        
        plan.add(new Header(10, 750, "2011-04-21"));
        plan.add(new Header(300, 750, "Devise and parameter range"));
        plan.add(new Header(600, 750, "Place description"));
        instanse = this;
    }
    
    public void drawGrid (Graphics g) {
        Color tmp = g.getColor();
        g.setColor(Color.GRAY.brighter());
        for (int i = 0; i < this.getWidth(); i += 10)
            g.drawLine(i, 0, i, this.getHeight());
        for (int i = 0; i < this.getHeight(); i += 10)
            g.drawLine (0, i, this.getWidth(), i);
        g.setColor(tmp);
    }
    
    @Override
    public void paint (Graphics g) {
        super.paint(g);
        ((Graphics2D)g).scale(scale, scale);
        if (grid)
            drawGrid(g);
        //because of backgrounds of each element in order not to erase some details shapes must be drown in that order
        //rooms first
        for (int i = 0;  i < plan.size(); i++)
            if (plan.get(i).type == Shape.ROOM)
            plan.get(i).draw((Graphics2D)g, Color.BLACK);
        //doors and windows (it will erase some wall parts
        for (int i = 0;  i < plan.size(); i++)
            if (plan.get(i).type == Shape.DOOR || plan.get(i).type == Shape.WINDOW)
            plan.get(i).draw((Graphics2D)g, Color.BLACK);
        //colums and parameters 
        for (int i = 0;  i < plan.size(); i++)
            if (plan.get(i).type == Shape.COLUMN || plan.get(i).type == Shape.PARAMETER || plan.get(i).type == Shape.RESIZE_SHAPE)
            plan.get(i).draw((Graphics2D)g, Color.BLACK);
        //complex shapes
        for (int i = 0;  i < plan.size(); i++)
            if (plan.get(i).type == Shape.COMPLEX_SHAPE)
            plan.get(i).draw((Graphics2D)g, currColor);        
        //also, buffer shape and curren shape must to be draw
        if (MainPanel.buffShape != null) {
            MainPanel.buffShape.draw((Graphics2D)g, currColor);
            
        }
        if (MainPanel.currShape != null) {
            if (MainPanel.currShape.type != Shape.ANGLE)              
                MainPanel.currShape.draw((Graphics2D)g, currColor);
            if (MainPanel.currShape.type == Shape.PARAMETER)
                g.drawString(MainPanel.currShape.description, MainPanel.currShape.x, MainPanel.currShape.y-2);
        }
        //after all draw headers
      
        for (int i = 0; i < plan.size(); i++)
            if (plan.get(i).type == Shape.HEADER){
                plan.get(i).draw((Graphics2D)g, Color.BLACK);
            }
        
    }
    
    
    
    public static Shape shapeCreator (int type, int x, int y) {
        if (type == Shape.WINDOW)
            return new Window(x, y, 0, 0);
        if (type == Shape.DOOR)
            return new Door(x, y, 0, 0);
        if (type == Shape.COLUMN)
            return new Column(x, y, 0, 0);
        if (type == Shape.ROOM)
            return new Room (x, y, 4, 4);
        if (type == Shape.COMPLEX_SHAPE)
            return new ComplexShape(x, y, 0, 0);        
        if (type == Shape.PARAMETER) {
          
           ParameterAddFrame frame = new ParameterAddFrame();
           frame.setVisible(true);
           
           Parameter result = frame.getResult();
           frame.dispose();
            if (result != null)
                 result.moveTo(x, y);
            return result;
            
        }
        return null;
    }
    public static void addShape (Shape shape) {
        if (shape.type != Shape.COMPLEX_SHAPE)
            plan.add(shape);
        else {
            if (((ComplexShape)shape).elements.isEmpty())
                return;
            for (int i = 0; i < plan.size(); i++)
                if (plan.get(i).type == Shape.COMPLEX_SHAPE)
                    plan.remove(i);
            plan.add(shape);
        }
    }

    public static MainPanel getInstanse () {
        if (instanse != null)
        return instanse;
        return new MainPanel();
    }
    
    public int getMinX () {
        int result = 100500;
        for (int i = 0; i < plan.size(); i++) 
            if (plan.get(i).x < result && plan.get(i).type == Shape.ROOM)
                result = plan.get(i).x;        
        return result;
    }
    
    public int getMinY () {
        int result = 100500;
        for (int i = 0; i < plan.size(); i++) 
            if (plan.get(i).y < result && plan.get(i).type == Shape.ROOM)
                result = plan.get(i).y;        
        return result;
    }
    
    public int getMaxX () {
        int result = 0;
        for (int i = 0; i < plan.size(); i++) 
            if ((plan.get(i).x + plan.get(i).width) > result && plan.get(i).type == Shape.ROOM)
                result = plan.get(i).x + plan.get(i).width;        
        return result;
    }
    
    public int getMaxY () {
        int result = 0;
        for (int i = 0; i < plan.size(); i++) 
            if ((plan.get(i).y + plan.get(i).height) > result && plan.get(i).type == Shape.ROOM)
                result = plan.get(i).y + plan.get(i).height;        
        return result;
    }
    
    public BufferedImage print(Graphics g, PageFormat pageFormat, int pageIndex) {
        int dx = getMinX();
        int dy = getMinY();
        BufferedImage result = new BufferedImage(getMaxX()-dx+1, getMaxY()-dy+1, BufferedImage.TYPE_INT_RGB);
      //    BufferedImage result = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        g = result.getGraphics();
        super.paint(g);
        ((Graphics2D)g).scale(scale, scale);
        if (grid)
            drawGrid(g);
        //because of backgrounds of each element in order not to erase some details shapes must be drown in that order
        //rooms first
        for (int i = 0;  i < plan.size(); i++)
            if (plan.get(i).type == Shape.ROOM)
            plan.get(i).print(result, dx, dy);
        //doors and windows (it will erase some wall parts
        for (int i = 0;  i < plan.size(); i++)
            if (plan.get(i).type == Shape.DOOR || plan.get(i).type == Shape.WINDOW)
            plan.get(i).print(result, dx, dy);
        //colums and parameters 
        for (int i = 0;  i < plan.size(); i++)
            if (plan.get(i).type == Shape.COLUMN || plan.get(i).type == Shape.PARAMETER || plan.get(i).type == Shape.RESIZE_SHAPE)
            plan.get(i).print(result, dx, dy); 
  
        return result;
    }
}
