/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 *
 * @author alexxx
 */
public class Parameter extends Shape{
    public double min;
    public double max;
    public double badMin;
    public double badMax;
    public double goodMin;
    public double goodMax;
    public double paramValue;
    static final int paramDiameter = 50;
    
    public Parameter (int x, int y, double min, double max, double badMin, double badMax, double goodMin, double goodMax, double paramValue) {
        this.x = x;
        this.y = y;
        this.min = min;
        this.max = max;
        this.badMin = badMin;
        this.badMax = badMax;
        this.goodMin = goodMin;
        this.goodMax = goodMax;
        this.paramValue = paramValue;
        this.width = paramDiameter;
        this.height = paramDiameter;
        this.type = Shape.PARAMETER;
        this.description = "";
    }
    @Override
    public void draw(Graphics2D g, Color color) {
        //seting some line antialiasing
        RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHints(renderHints);
       //rotate to angle (angle in radians, plus is clock wise)       
       g.rotate(this.angle, this.x+this.width/2, this.y+this.height/2);
        //set coordinats to element begin
        g.translate(this.x, this.y);        
       //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }      
       //draw element 
       //finding where parameter value is
       g.setColor(Color.RED);
       //if (this.paramValue < this.max && this.paramValue > this.min)
          
       if (this.paramValue < this.badMax && this.paramValue > this.badMin)
          g.setColor(Color.YELLOW);
       if (this.paramValue < this.goodMax && this.paramValue > this.goodMin)
          g.setColor(Color.GREEN);
       g.fillOval(0, 0, paramDiameter, paramDiameter);
       g.setColor(Color.BLACK);
       g.drawString(this.paramValue+"", paramDiameter/2-7, paramDiameter/2+5);
       //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }
       g.translate(-this.x, -this.y); 
       //returning coordinats to the (0, 0), 0 radians
       g.rotate(-this.angle, this.x+this.width/2, this.y+this.height/2);
        
     
    }
}