/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class Room extends Shape{
    public static final int wallSize = 6;
    private  ArrayList<Angle> angles = new ArrayList<Angle> ();
    
    public Room () {
        this.type = Shape.ROOM;
    }
    
    public Room (int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.angles.add(new Angle(0, 0, wallSize, wallSize, this));
        this.angles.add(new Angle(0, height-wallSize, wallSize, wallSize, this));
        this.angles.add(new Angle(width-wallSize, height-wallSize, wallSize, wallSize, this));
        this.angles.add(new Angle(width-wallSize, 0, wallSize, wallSize, this));
        
        this.type = Shape.ROOM;
    }
    @Override
    public void draw(Graphics2D g, Color color) {
        //seting some line antialiasing
        RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHints(renderHints);
       
        //set coordinats to element begin
        g.translate(this.x, this.y);        
       //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }
       
       //rotate to angle (angle in radians, plus is clock wise)       
       g.rotate(this.angle, this.x+this.width/2, this.y+this.height/2);
       //draw element 
       g.setColor(color);
       Stroke b = g.getStroke();
       for (int i = 0; i < this.angles.size()-1; i++) {           
           g.setStroke(new BasicStroke(wallSize));
           g.drawLine(this.angles.get(i).x+wallSize/2, this.angles.get(i).y+wallSize/2, this.angles.get(i+1).x+wallSize/2, this.angles.get(i+1).y+wallSize/2);
           g.setStroke(new BasicStroke(wallSize-2));
           g.setColor(Color.WHITE);
           g.drawLine(this.angles.get(i).x+wallSize/2, this.angles.get(i).y+wallSize/2, this.angles.get(i+1).x+wallSize/2, this.angles.get(i+1).y+wallSize/2);
           g.setColor(color);
       }
       g.setStroke(new BasicStroke(wallSize));
       g.drawLine(this.angles.get(0).x+wallSize/2, this.angles.get(0).y+wallSize/2, this.angles.get(this.angles.size()-1).x+wallSize/2, this.angles.get(this.angles.size()-1).y+wallSize/2);
       g.setStroke(new BasicStroke(wallSize-2));
       g.setColor(Color.WHITE);
       g.drawLine(this.angles.get(0).x+wallSize/2, this.angles.get(0).y+wallSize/2, this.angles.get(this.angles.size()-1).x+wallSize/2, this.angles.get(this.angles.size()-1).y+wallSize/2);
       g.setColor(color);
       g.setStroke(b);
       for (int i = 0; i < this.angles.size(); i++)
           this.angles.get(i).draw(g, color);
       
       //returning coordinats to the (0, 0), 0 radians
        g.rotate(-this.angle, this.x+this.width/2, this.y+this.height/2);
        
        //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }
        
        g.translate(-this.x, -this.y);
    }

    @Override
    public void calcWidthHeight () {
        int w = 0;
        int h = 0;
        //find negative values (if there is)        
        for (int i = 0; i < this.angles.size(); i++) {
            if (this.angles.get(i).x < w)
                w = this.angles.get(i).x;
            if (this.angles.get(i).y < h)
                h  = this.angles.get(i).y;
        }
        //correcing angle coordinats
        if (w < 0) {
            for ( int i = 0; i < this.angles.size(); i++)
               this.angles.get(i).x += -w;
            this.x += w;
        }
        if (h < 0) {
            for ( int i = 0; i < this.angles.size(); i++)
               this.angles.get(i).y += -h;
            this.y += h;
        }
        //finding max x and max y 
        for (int i = 0; i < this.angles.size(); i++) {
            if (this.angles.get(i).x > w)
                w = this.angles.get(i).x;
            if (this.angles.get(i).y > h)
                h = this.angles.get(i).y;
        }
        //max x == width, max y == height
        this.width = w+wallSize;
        this.height = h+wallSize;
    }
    
    @Override
    public boolean isOnShape (int x, int y) {
        int dx = x - this.x;
        int dy = y - this.y;
        if (dx >= 0 && dy >= 0)
            return isOnWalls(dx, dy);
        return false;
    }
    
    public boolean isOnWalls (int x, int y) {
        double a = (this.angles.get(0).y+wallSize/2) - (this.angles.get(this.angles.size()-1).y+wallSize/2);
        double b = (this.angles.get(this.angles.size()-1).x+wallSize/2) - (this.angles.get(0).x+wallSize/2);
        double c = (this.angles.get(0).x+wallSize/2)*
                   (this.angles.get(this.angles.size()-1).y+wallSize/2)-
                   (this.angles.get(this.angles.size()-1).x+wallSize/2)*
                   (this.angles.get(0).y+wallSize/2);
        double d = Math.abs((a*x+b*y+c)/Math.sqrt(a*a+b*b));
        if (d <= wallSize/2)
            return true;
        
        for (int i = 0; i < this.angles.size()-1; i++) {
             a = (this.angles.get(i).y+wallSize/2) - (this.angles.get(i+1).y+wallSize/2);
             b = (this.angles.get(i+1).x+wallSize/2) - (this.angles.get(i).x+wallSize/2);
             c = (this.angles.get(i).x+wallSize/2)*(this.angles.get(i+1).y+wallSize/2)-(this.angles.get(i+1).x+wallSize/2)*(this.angles.get(i).y+wallSize/2);
             d = Math.abs((a*x+b*y+c)/Math.sqrt(a*a+b*b));
             if (d <= wallSize/2)
                return true;               
        }   
        return false;
    }
    
    public void addAngle (Angle newAngle) {
        double a = (this.angles.get(0).y+wallSize/2) - (this.angles.get(this.angles.size()-1).y+wallSize/2);
        double b = (this.angles.get(this.angles.size()-1).x+wallSize/2) - (this.angles.get(0).x+wallSize/2);
        double c = (this.angles.get(0).x+wallSize/2)*
                   (this.angles.get(this.angles.size()-1).y+wallSize/2)-
                   (this.angles.get(this.angles.size()-1).x+wallSize/2)*
                   (this.angles.get(0).y+wallSize/2);
        double d = Math.abs((a*newAngle.x+b*newAngle.y+c)/Math.sqrt(a*a+b*b));
        if (d <= wallSize/2) {
            this.angles.add(newAngle);
            return;
        }            
        for (int i = 0; i < this.angles.size()-1; i++) {
             a = (this.angles.get(i).y+wallSize/2) - (this.angles.get(i+1).y+wallSize/2);
             b = (this.angles.get(i+1).x+wallSize/2) - (this.angles.get(i).x+wallSize/2);
             c = (this.angles.get(i).x+wallSize/2)*(this.angles.get(i+1).y+wallSize/2)-(this.angles.get(i+1).x+wallSize/2)*(this.angles.get(i).y+wallSize/2);
             d = Math.abs((a*newAngle.x+b*newAngle.y+c)/Math.sqrt(a*a+b*b));
             if (d <= wallSize/2) {
                this.angles.add(i+1, newAngle);
                return;
            }   
        }   
        calcWidthHeight();
    }
      
    public void setAngle (Angle newAngle) {
        this.angles.add(newAngle);
        calcWidthHeight();
    }
    public Angle getAngle (int x, int y) {
        for (int i = 0; i< this.angles.size(); i++) 
            if ((Math.abs(x-this.angles.get(i).x) <= wallSize) && (Math.abs(y-this.angles.get(i).y) <= wallSize))
                    return this.angles.get(i);
        return null;
    }
    
    public int getNumberOfAngles () {
        return this.angles.size();
    }
    
    public Angle getAngle (int index) {
        return this.angles.get(index);
    }
    
    @Override
    public void setWidth (int width) {
       // System.out.println ("***TEST DATA FROM ROOM CLASS***");
     //   System.out.println ("THIS.WIDTH == "+this.width);
     //   System.out.println ("WIDTH == "+width);
     //   System.out.println ("WALLSIZE == "+wallSize);
        for (int i = 0; i < this.angles.size(); i++) {
            if (this.angles.get(i).x == this.width-wallSize)
                this.angles.get(i).x = width-wallSize;
        }
        this.width = width;
    }
    
    @Override
    public void setHeight (int height) {
        for (int i = 0; i < this.angles.size(); i++)
            if (this.angles.get(i).y == this.height-wallSize)
                this.angles.get(i).y = height-wallSize;
        this.height = height;
    }
}