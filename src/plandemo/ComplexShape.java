/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class ComplexShape extends Shape{

    public ArrayList<Shape> elements = new ArrayList<Shape>();
    
    public ComplexShape (int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.type = Shape.COMPLEX_SHAPE;
        addElements();
    }
    
    public void addElements () {
        this.elements = new ArrayList<Shape>();
        int thisEndX = this.x+this.width;
        int thisEndY = this.y+this.height;
        for (int i = 0; i < MainPanel.plan.size(); i++) {
            int currShapeMiddleX = MainPanel.plan.get(i).x+MainPanel.plan.get(i).width/2;
            int currShapeMiddleY = MainPanel.plan.get(i).y+MainPanel.plan.get(i).height/2;
            if (currShapeMiddleX >= this.x && 
                currShapeMiddleX <= thisEndX &&
                currShapeMiddleY >= this.y && 
                currShapeMiddleY <= thisEndY)                
                this.elements.add(MainPanel.plan.get(i));                        
        }            
        for (int i = 0; i < this.elements.size(); i++)
            if (this.elements.get(i).type == Shape.ROOM)
                if (this.elements.get(i).x < this.x &&
                    this.elements.get(i).y < this.y &&
                    this.elements.get(i).width > this.width &&
                    this.elements.get(i).height > this.height)
                    this.elements.remove(i);
    }

    @Override
    public void setWidth (int width) {
        super.setWidth(width);
        addElements();
    }
    
    @Override
    public void setHeight (int height) {
        super.setHeight(height);
        addElements();
    }
    
    @Override
    public void draw(Graphics2D g, Color color) {
        //draw all selected elements
         for (int i = 0; i < this.elements.size(); i++) 
            this.elements.get(i).draw(g, Color.BLUE);
              //seting some line antialiasing
        RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHints(renderHints);
       //rotate to angle (angle in radians, plus is clock wise)       
       g.rotate(this.angle, this.x+this.width/2, this.y+this.height/2);
        //set coordinats to element begin
        g.translate(this.x, this.y);        
       //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }      
       //draw element 
        g.setColor(color);
       
        float[] dashPattern = {2.0f, 5.0f};
        g.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 10.0f, dashPattern, 0));  
        g.drawRect(0, 0, this.width, this.height); 
        g.setStroke(new BasicStroke(1));

       //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }
       g.translate(-this.x, -this.y); 
       //returning coordinats to the (0, 0), 0 radians
       g.rotate(-this.angle, this.x+this.width/2, this.y+this.height/2);
    }
 
    @Override
    public void moveTo (int x, int y) {
        int dx = this.x - x;
        int dy = this.y - y;
        for (int i = 0; i< this.elements.size(); i++){
            this.elements.get(i).x -= dx;
            this.elements.get(i).y -= dy;
        }
        this.x = x;
        this.y = y;
    }
}
