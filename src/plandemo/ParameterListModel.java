/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.util.ArrayList;
import javax.swing.DefaultListModel;

/**
 *
 * @author alexxx
 */
public class ParameterListModel extends DefaultListModel {

    public ArrayList<Parameter> parameterList = MainPanel.parameters;
    
    @Override
    public int getSize() {
        return this.parameterList.size();        
    }

    @Override
    public Object getElementAt(int index) {
        String result = this.parameterList.get(index).description+
                        ": (Min; Max) : ( "+this.parameterList.get(index).min+"; "+this.parameterList.get(index).max+
                        ");  (Good min; Good max) : ( "+this.parameterList.get(index).badMin+"; "+this.parameterList.get(index).badMax+
                        "); (Normal min; Normal max) : ( "+this.parameterList.get(index).goodMin+"; "+this.parameterList.get(index).goodMax+")";
        return result;        
    }

    @Override
    public void removeElementAt (int index)  {
        this.parameterList.remove(index);
    }
}
