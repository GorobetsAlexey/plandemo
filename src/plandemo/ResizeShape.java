/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class ResizeShape extends Shape{
    public Shape resizedShape;
    public ArrayList <Angle> resizeMarkers = new ArrayList<Angle>();
    
    public ResizeShape (Shape shape) {
        this.resizedShape = shape;
        this.x = shape.x;
        this.y = shape.y;
        this.width = shape.width;
        this.height = shape.height;
        this.resizeMarkers.add(new Angle(-Room.wallSize, -Room.wallSize, Room.wallSize, Room.wallSize, this));
        this.resizeMarkers.add(new Angle(this.width, -Room.wallSize, Room.wallSize, Room.wallSize, this));
        this.resizeMarkers.add(new Angle(-Room.wallSize, this.height, Room.wallSize, Room.wallSize, this));
        this.resizeMarkers.add(new Angle(this.width, this.height, Room.wallSize, Room.wallSize, this));
        this.type = Shape.RESIZE_SHAPE;
    }
    
    @Override 
    public boolean isOnShape (int x, int y){
        for (int i = 0; i < this.resizeMarkers.size(); i++)
            if (this.resizeMarkers.get(i).isOnShape(x-this.x, y-this.y))
                return true;
        return false;
    }
    
    public Angle getMarker (int x, int y) {
        for (int i = 0; i< this.resizeMarkers.size(); i++) 
            if ((Math.abs(x-(this.resizeMarkers.get(i).x+this.x)) <= Room.wallSize) && (Math.abs(y-(this.resizeMarkers.get(i).y+this.y)) <= Room.wallSize))
                    return this.resizeMarkers.get(i);
        return null;
    }
    
    @Override
    public BufferedImage print(BufferedImage img, int xDif, int yDif) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void draw(Graphics2D g, Color color) {
         //seting some line antialiasing
        RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHints(renderHints);
       //rotate to angle (angle in radians, plus is clock wise)       
       g.rotate(this.angle, this.x+this.width/2, this.y+this.height/2);
        //set coordinats to element begin
        g.translate(this.x, this.y);        
       //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }      
       //draw element 
        g.setColor(color);       
        float[] dashPattern = {2.0f, 5.0f};
        g.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 10.0f, dashPattern, 0));  
        g.drawRect(0, 0, this.width, this.height); 
        g.setStroke(new BasicStroke(1));
        
        g.drawRect(-Room.wallSize, -Room.wallSize, Room.wallSize, Room.wallSize);
        g.drawRect(this.width, -Room.wallSize, Room.wallSize, Room.wallSize);
        g.drawRect(-Room.wallSize, this.height, Room.wallSize, Room.wallSize);
        g.drawRect(this.width, this.height, Room.wallSize, Room.wallSize);
       //for (int i = 0; i < this.resizeMarkers.size(); i++)
       //   this.resizeMarkers.get(i).draw(g, color);
       // this.resizedShape.draw(g, color);
        
       //horisontal flip/
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }
       g.translate(-this.x, -this.y); 
       //returning coordinats to the (0, 0), 0 radians
       g.rotate(-this.angle, this.x+this.width/2, this.y+this.height/2);
    }
    
    @Override
    public void calcWidthHeight () {
       this.resizeMarkers = new ArrayList<Angle>();
       this.resizeMarkers.add(new Angle(-Room.wallSize, -Room.wallSize, Room.wallSize, Room.wallSize, this));
        this.resizeMarkers.add(new Angle(this.width, -Room.wallSize, Room.wallSize, Room.wallSize, this));
        this.resizeMarkers.add(new Angle(-Room.wallSize, this.height, Room.wallSize, Room.wallSize, this));
        this.resizeMarkers.add(new Angle(this.width, this.height, Room.wallSize, Room.wallSize, this));
        
        
        this.resizedShape.x = this.x;
        this.resizedShape.y = this.y;
        this.resizedShape.width = this.width;
        this.resizedShape.height = this.height;
    }
    
    public int getFarestX (int x) {
        int result = 0;
        int w = 0;
        for (int i = 0; i < this.resizeMarkers.size(); i++)
            if (w < Math.abs(Math.max (0, this.resizeMarkers.get(i).x) +this.x - x)) {
                w = Math.abs(Math.max (0, this.resizeMarkers.get(i).x) +this.x - x);
                result = Math.max (0, this.resizeMarkers.get(i).x)+this.x;
            }
        return result;
    }
    
    public int getFarestY (int y) {
        int result = 0;
        int h = 0;
        for (int i = 0; i < this.resizeMarkers.size(); i++)
            if (h < Math.abs(Math.max (0, this.resizeMarkers.get(i).y)+this.y- y)) {
                h = Math.abs(Math.max (0, this.resizeMarkers.get(i).y)+this.y- y);
                result = Math.max (0, this.resizeMarkers.get(i).y)+this.y;
            }
        return result;
    }
}
