/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 *
 * @author alexxx
 */
public abstract class Shape implements Serializable{
    //types of draw elements;
    static int SHAPE = 0;
    static int ROOM = 1;
    static int DOOR = 2;
    static int WINDOW = 3;
    static int COLUMN = 4;
    static int ANGLE = 5;
    static int PARAMETER = 6;
    static int HEADER = 7;
    static int COMPLEX_SHAPE = 8;
    static int RESIZE_SHAPE = 9;
    //flip axis
    static int X_AXIS = 1;
    static int Y_AXIS = 2;
    static int X_Y_AXIS = 3;
    static int NONE = 0;
    //data fields
    public double angle;
    public int x;
    public int y;
    public int width;
    public int height;
    public String description;
    public int type;
    public int flip;
       
    public Shape () {
        
    }
    
    public Shape (int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public Shape (int x, int y, int type){
        this.x = x;
        this.y = y;
        this.type = type;
    }
    
    public void calcWidthHeight () {};
    
    public boolean isOnShape (int x, int y) {
        if (x > this.x && x < this.x+width && y > this.y && y < this.y+height)
            return true;
        return false;
    }
    //print element on the page
    public BufferedImage print(BufferedImage img, int xDif, int yDif) {
        Graphics2D g = (Graphics2D)img.getGraphics();
        this.x -= xDif;
        this.y -= yDif;
        this.draw(g, Color.BLACK);
        this.x += xDif;
        this.y += yDif;
     return img;   
    }
    
    //rotate shape by angle
    public void rotate (double angle) {
        this.angle = angle;
    }
    
    //draw shape at main panel
    public abstract void draw (Graphics2D g, Color color);
    
    //move shape to (x;y)
    public void moveTo (int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void setWidth (int width) {
      this.width = width;  
    }
    
    public void setHeight (int height) {
      this.height = height;  
    }
    
    //set flip axis 
    public void flip (int axis) {
        if (this.flip == axis){
            this.flip = Shape.NONE;
            return;
        }
        if (this.flip == Shape.NONE){
            this.flip = axis;
            return;
        }
        else {
            if ((this.flip == Shape.X_AXIS && axis == Shape.Y_AXIS)||(this.flip == Shape.Y_AXIS && axis == Shape.X_AXIS)){
                this.flip = Shape.X_Y_AXIS;
                return;
            }
            if (this.flip == Shape.X_Y_AXIS && axis == Shape.X_AXIS){
                this.flip = Shape.Y_AXIS;
                return;
            }
            if (this.flip == Shape.X_Y_AXIS && axis == Shape.Y_AXIS){
                this.flip = Shape.X_AXIS;
                return;
            }            
        }
    }
}
