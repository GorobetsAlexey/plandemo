/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;


/**
 *
 * @author alexxx
 */
public class Header extends Shape{
    public String content = "";
    
    public Header (int x, int y, String content) {
        this.x = x;
        this.y = y;
        this.content = content;
        this.type = Shape.HEADER;
    }
    
    @Override
    public boolean isOnShape (int x, int y) {
        if (x > this.x && x < this.x+width && y < this.y && y > this.y-height)
            return true;
        return false;
    }
    
    @Override
    public void draw(Graphics2D g, Color color) {
        g.drawString(content, x, y);
        this.width = g.getFont().getSize()*this.content.length();
        this.height = g.getFont().getSize();
    }

    @Override
    public BufferedImage print(BufferedImage img, int xDif, int yDif) {
        throw new UnsupportedOperationException("Not supported yet.");
    }    
}