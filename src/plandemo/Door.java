/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/**
 *
 * @author alexxx
 */
public class Door extends Shape{
    
    public Door (int x, int y, int width, int height){
        super ();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.type = Shape.DOOR;
    }
    @Override
    public void draw(Graphics2D g, Color color) {
        //seting some line antialiasing
        RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHints(renderHints);
        //rotate to angle (angle in radians, plus is clock wise)       
       g.rotate(this.angle, this.x+this.width/2, this.y+this.height/2);
        //set coordinats to element begin
        g.translate(this.x, this.y);        
       //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }
       
      
       //draw element 
       g.setColor(Color.WHITE);
       g.fillRect(0, 0, this.width, this.height);
       g.setColor(color);
       g.drawLine(0, this.height/2, this.width, this.height/2);
       g.drawLine(0, 1,0, this.height);
       g.drawLine(this.width, 1, this.width, this.height);
       
       int h = new Double(Math.tan(Math.PI*15/180)*this.width).intValue();
       g.drawLine(0, this.height/2, this.width-this.height/3, -h);
              //returning coordinats to the (0, 0), 0 radians
       
        
        //horisontal flip
       if (this.flip == Shape.Y_AXIS) {
        int m = this.height/2;
        g.translate(0,m); 
        g.scale(1,-1);
        g.translate(0,-m);
       }
       //vertical flip
       if (this.flip == Shape.X_AXIS) {
        int m = this.width/2;
        g.translate(m, 0); 
        g.scale(-1,1);
        g.translate(-m, 0);
      }
       //both x and y flip
       if (this.flip == Shape.X_Y_AXIS) {
        int m = this.width/2;
        int n = this.height/2;
        g.translate(m, n); 
        g.scale(-1,-1);
        g.translate(-m, -n);
       }
        g.translate(-this.x, -this.y); 
         g.rotate(-this.angle, this.x+this.width/2, this.y+this.height/2);
        
        
  
    }
}