/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plandemo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 *
 * @author alexxx
 */
public class PrintWarper  implements Printable{
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
       double height = pageFormat.getImageableHeight();
       double width = pageFormat.getImageableWidth();
             
       BufferedImage image = MainPanel.getInstanse().print(null, null, 0);
       
       //caculating number of pages
       int pageNumbers = 0;
       Double heightPages = new Double(((double)image.getHeight())/height);
       Double widthPages = new Double(((double)image.getWidth())/width);
       int hp = heightPages.intValue();
       pageNumbers = hp;
       if ((heightPages - heightPages.intValue()) > 0)
           pageNumbers++;
       int wp = widthPages.intValue();
       if ((widthPages - widthPages.intValue()) > 0)
           wp++;
       
       pageNumbers *= wp;
              
       if (pageIndex >= pageNumbers)
                return NO_SUCH_PAGE; 
       
       Graphics2D g2d = (Graphics2D)graphics;
       BufferedImage bufImg = null;
       bufImg = image.getSubimage((pageIndex%wp)*new Double (width).intValue(), 
                                  (pageIndex/wp)*new Double (height).intValue(),
                                   Math.min(new Double (width).intValue(), 
                                            (image.getWidth() - ((pageIndex%wp))*new Double (width).intValue())),
                                   Math.min(new Double (height).intValue(), 
                                            (image.getHeight() - ((pageIndex/wp))*new Double (height).intValue()))
                                 );
    // String file = pageIndex+".jpg";
       
       if (bufImg.getWidth() < new Double (width).intValue() || bufImg.getHeight() < new Double (height).intValue()) {
           BufferedImage tmp = new BufferedImage (new Double (width).intValue(), new Double (height).intValue(), BufferedImage.TYPE_INT_RGB);
           tmp.getGraphics().setColor(Color.WHITE);
           tmp.getGraphics().fillRect(0, 0, tmp.getWidth(), tmp.getHeight());
           tmp.getGraphics().drawImage(bufImg, 0, 0, null);
           bufImg = tmp;
       }
     /*  try {
				ImageIO.write(bufImg, "JPEG", new File(file));
				
			} catch (IOException exception) {
				System.out.print(exception);
			}		
         
		*/	
       g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());		
       graphics.drawImage(bufImg, 0, 0, null);
       return PAGE_EXISTS;
    }
    
}
